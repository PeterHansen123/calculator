let calculatedResult = document.getElementById("result");
const grid = document.getElementById("grid-1");
const equal = document.getElementById("equal");
let firstNumber = [];
let secondNumber = [];
let operator = [];
let result;

grid.addEventListener("keydown", (event) => {
  if (
    event.isComposing ||
    event.keyCode === 229 ||
    !/[0-9-+*" "./\/]+/.test(event.key)
  ) {
    return;
  }
  if (/[0-9.\/]+/.test(event.key)) {
    if (operator == 0) {
      firstNumber += event.key;
      calculatedResult.innerText = firstNumber;
    } else {
      secondNumber += event.key;
      calculatedResult.innerText = secondNumber;
    }
  }
  if (/[-+*/\/]+/.test(event.key)) {
    operator = event.key;
    calculatedResult.innerText = operator;
  }
  if (event.key === " ") {
    event.preventDefault();
  }
});

function numberFunc(value) {
  if (operator == 0) {
    firstNumber += value;
    calculatedResult.innerText = firstNumber;
  } else {
    secondNumber += value;
    calculatedResult.innerText = secondNumber;
  }
}

function operatorFunc(value) {
  operator = value;
  calculatedResult.innerText = operator;
}

grid.addEventListener("keydown", function(event) {
  if (event.key === "Enter") {
    event.preventDefault();
    equal.click();
  } 
});

function calculate() {
  switch (operator) {
    case "+":
      result = parseFloat(firstNumber) + parseFloat(secondNumber);
      break;
    case "-":
      result = firstNumber - secondNumber;
      break;
    case "*":
      result = firstNumber * secondNumber;
      break;
    case "/":
      result = firstNumber / secondNumber;
      break;
  }

  calculatedResult.innerText = result.toFixed(2).replace(/[.,]00$/, "");
  firstNumber = [];
  secondNumber = [];
  operator = [];
}

function deleteFunc() {
  firstNumber = [];
  secondNumber = [];
  operator = [];
  calculatedResult.innerText = "";
}
